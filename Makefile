check_clean: check clean

check:
	./tests/run.sh time

clean:
	find ./tests/*.input/ -name '*.html' -delete -o -name 'feed.*' -delete

distclean: clean
	rm -rf *.egg-info/ dist/ build/ README

expectations:
	./tests/run.sh update

release: distclean
	cp README.md README
	python3 setup.py sdist
	twine upload dist/*
