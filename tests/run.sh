#!/bin/sh -eu

SCRIPT=$(readlink -f $0)
TESTDIR=$(dirname $SCRIPT)
ROOTDIR=$(dirname $TESTDIR)
TIME="Tdd:dd:ddZ"
PATTERN=$(echo $TIME | sed 's/d/[[:digit:]]/g')
TODAY=$(date +%Y-%m-%d)
DISPLAY_TODAY=$(date '+%A, %B %e, %Y')
ACTION=""

[ "$#" = "1" ] && ACTION="$1"

for input in $TESTDIR/*.input/; do
    cd $input

    hy $ROOTDIR/static.hy

    files=$(find $input -name '*.html' -o -name 'feed.*' | sort)
    sed -i "s/$PATTERN/T12:34:56Z/" $files
    sed -i "s/$TODAY/____TODAY____/" $files
    sed -i "s/$DISPLAY_TODAY/____DISPLAY_TODAY____/" $files

    for check in $files; do
        expected=$(echo $check | sed 's/.input/.expected/')
        [ "$ACTION" = "update" ] && cp $check $expected
        diff -Naur $expected $check
        echo Passed: $check
    done
done

[ "$ACTION" = "time" ] && echo Succeeded in $(ps --pid $$ --format etime=)s
exit 0
